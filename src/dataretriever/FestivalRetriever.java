package dataretriever;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

/**
 * @author danilo
 *
 */
public class FestivalRetriever {

	private List<Festival> festivals;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FestivalRetriever fr = new FestivalRetriever();
		fr.retrieveAllFestivals();
		try {
			FileWriter fw = new FileWriter("textfiles/festivals.txt");
			PrintWriter pw = new PrintWriter(fw);
			for (Festival f : fr.festivals) {
				pw.println(f.toStringForFile());
				System.out.println(f.toStringForFile());
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void retrieveAllFestivals(){
		festivals = new ArrayList<Festival>();
		try {
			String homeURL = "http://www.festivalhopper.de";
	        URL oracle = new URL(homeURL + "/festivals-2014.php");
	        URLConnection yc = oracle.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(
	                                yc.getInputStream()));
	        String inputLine;
	        Boolean isFestivalBlock = false;
	        int festivalNumber = 0;
	        while ((inputLine = in.readLine()) != null){
	        	
	        	if (isFestivalBlock || inputLine.contains("<div class=\"block\">")){
	        		isFestivalBlock = true;		    
	        		String location = "";
	        		if (inputLine.contains("<span class=\"l\">")){
	        			try {
	        				String cutOfPart = "<span class=\"l\">";
	        				String restOfInputLine = inputLine.substring(inputLine.indexOf(cutOfPart));
	        				restOfInputLine = restOfInputLine.substring(cutOfPart.length());
	        				if (restOfInputLine.contains("<span class=\"location\">")){
	        					cutOfPart = "<span class=\"location\">";
	        					restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
	        					restOfInputLine = restOfInputLine.substring(cutOfPart.length());
	        					cutOfPart = "</span>";

	        					location = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
	        					inputLine = in.readLine();
	        				} else {	        	
	        					cutOfPart = " <span class";
	        					location = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
	        					inputLine = in.readLine();
	        				}
	        			} catch (Exception e){
				            System.out.println("############error############");
				            System.out.println(e.toString());
				            System.out.println("############error############");
	        			}
	        		}
	        		if (inputLine != null && inputLine.contains("<span class=\"d\">")){
	        			try {
		        			String cutOfPart = "<span class=\"d\">";
		        			String restOfInputLine = inputLine.substring(inputLine.indexOf(cutOfPart));
		        			restOfInputLine = restOfInputLine.substring(cutOfPart.length());
		        			cutOfPart = "</span> - <a ";
		        			
		        			String date = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
		        			
		        			restOfInputLine = restOfInputLine.substring(date.length() + cutOfPart.length());
			        		cutOfPart = "href=\"";
			        		restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
		        			restOfInputLine = restOfInputLine.substring(cutOfPart.length());
			        		cutOfPart = "\"";
			        		
			        		String link = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
			        		
			        		restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
			        		cutOfPart = ">";
			        		restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
		        			restOfInputLine = restOfInputLine.substring(cutOfPart.length());
		        			cutOfPart = "<";
		        			
		        			String name = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
		        			festivals.add(new Festival(festivalNumber, date, name, location, homeURL + link));
		        			festivalNumber++;
						} catch (Exception e) {
				            System.out.println("############error############");
				            System.out.println(e.toString());
				            System.out.println("############error############");
						}	
	        		}
	        		if (inputLine.contains("</p></div>")){
	        			isFestivalBlock = false;
	        		}
	            }
	        }
	        in.close();			
		} catch (Exception e) {
            System.out.println("error");
		}
	}
	
	
}
