package dataretriever;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


/**
 * @author danilo
 *
 */
public class Festival {

	private int _festival_id;
	private String _date;
	private String _name;
	private String _location;
	private String _link;
	
	public String[] tags;

	public Festival(){
		_festival_id = -1;
		_date = "";
		_name = "";
		_location = "";
		_link = "";
	}
	public Festival(int id, String date, String name, String location, String link){
		_festival_id = id;
		_date = date;
		_name = name;
		_location = location;
		_link = link;
	}
	
	public String toString() {
		if (_date.length() > 4){
			return _date + "\t - " + _name + "; Link: " + _link;
		} else {
			return _date + "\t\t - " + _name + "; Link: " + _link;
		}
	}
	
	/**
	 * 
	 * @return festival_id,name,location,date,_link
	 */
	public String toStringForFile() {
		return _festival_id + "," + _name + "," + _location + "," + _date + "," + _link;
	}

	public void retrieveFestivalData(){
		try {
			URL oracle = new URL(_link);
			URLConnection yc = oracle.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					yc.getInputStream()));
			String inputLine;
			Boolean isFestivalInfoBlock = false;
			Boolean isFestivalGeneralBlock = false;
			Boolean isFestivalLineUpBlock = false;
			while ((inputLine = in.readLine()) != null){
				if (isFestivalInfoBlock || inputLine.contains("<div class=\"festivalinfos\">")){
					isFestivalInfoBlock = true;
					System.out.println(inputLine);

//					if (inputLine.contains("<span class=\"d\">")){
//						try {
//							String cutOfPart = "<span class=\"d\">";
//							String restOfInputLine = inputLine.substring(inputLine.indexOf(cutOfPart));
//							restOfInputLine = restOfInputLine.substring(cutOfPart.length());
//							cutOfPart = "</span> - <a ";
//							String date = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
//							restOfInputLine = restOfInputLine.substring(date.length() + cutOfPart.length());
//							cutOfPart = "href=\"";
//							restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
//							restOfInputLine = restOfInputLine.substring(cutOfPart.length());
//							cutOfPart = "\"";
//							String link = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
//							restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
//							cutOfPart = ">";
//							restOfInputLine = restOfInputLine.substring(restOfInputLine.indexOf(cutOfPart));
//							restOfInputLine = restOfInputLine.substring(cutOfPart.length());
//							cutOfPart = "<";
//							String name = restOfInputLine.substring(0, restOfInputLine.indexOf(cutOfPart));
//							festivals.add(new Festival(date, name, link));
//							System.out.println(date + " - " + name + "; Link: " + homeURL + link);
//
//						} catch (Exception e) {
//							System.out.println("############error############");
//							System.out.println(e.toString());
//							System.out.println("############error############");
//						}	
//					}
					if (inputLine.contains("</div>")){
						isFestivalInfoBlock = false;
					}
				}
				if (isFestivalGeneralBlock || inputLine.contains("<a name=\"infos\"></a>")){
					isFestivalInfoBlock = false;
					isFestivalGeneralBlock = true;
					System.out.println(inputLine);
					if (inputLine.contains("<a name=\"lineup\"></a>")){
						isFestivalGeneralBlock = false;
					}
				}
				if (isFestivalLineUpBlock || inputLine.contains("<a name=\"lineup\"></a>")){
					isFestivalInfoBlock = false;
					isFestivalGeneralBlock = false;
					isFestivalLineUpBlock = true;
					System.out.println(inputLine);
					if (inputLine.contains("</div>")){
						isFestivalLineUpBlock = false;
					}
				}
			}
			in.close();			
		} catch (Exception e) {
			System.out.println("error");
		}
	}

}
