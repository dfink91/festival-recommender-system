package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.CachingRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import au.com.bytecode.opencsv.CSVReader;

public class MainInterface extends JFrame implements ActionListener {

	private final static String USER_PREFERENCES_FILE = "textfiles/userpreferences2.txt";
	private final static String FESTIVAL_FILE = "textfiles/festivals.txt";
	private int currentUser;
	private String currentRating;
	private String[][] topTableContent = new String[0][0];
	private String[][] bottomTableContent = new String[5][3];
	private final static int NUMBER_OF_RECS = 5;
	private int[] recs = new int[NUMBER_OF_RECS];
	private Recommender recommender;
	private HashMap<String, String> mapUserPreferences;
	private int currentRow = 0;
	private ListSelectionModel listSelectionModel;
	private final JLabel lRateFestival = new JLabel();
	private JRadioButton[] radioButtons = new JRadioButton[6];
	private JScrollPane scrollPane2;

	public static void main(String args[]) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				initGUI();
			}
		});
	}

	public MainInterface(String name) {
		super(name);
		topTableContent = getTableContent(FESTIVAL_FILE);
		mapUserPreferences = new HashMap<>();
		userSimilarity(1);
	}

	private static void initGUI() {
		MainInterface frame = new MainInterface(
				"Festival Recommender System 2014");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.currentUser = -1;
		int numUsers = 0;
		try {
			numUsers = frame.recommender.getDataModel().getNumUsers();
		} catch (TasteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        LoginDialog loginDlg = new LoginDialog(frame, numUsers);
        loginDlg.setVisible(true);      
        if(loginDlg.isSucceeded()){
    		// Set up the content pane.
        	System.out.println("login successful");
        	frame.currentUser = Integer.parseInt(loginDlg.getUsername());
        	frame.mapUserPreferences = getUserpreferencesFromFile(USER_PREFERENCES_FILE);
        	frame.addComponentsToPane(frame.getContentPane());
    		// Display the window.
    		frame.setSize(500, 630);
    		frame.setResizable(false);
    		frame.setVisible(true);
    		frame.updateRecommendedFestivals();
        }
        if(loginDlg.isCanceled() || frame.currentUser == -1){
        	//determine program
        	System.exit(1);
        }
	}

	/**
	 * 
	 * @param textfile
	 * @return HashMap uId_fId | rating
	 */
	private static HashMap<String, String> getUserpreferencesFromFile(
			String textfile) {
		HashMap<String, String> map = new HashMap<>();
		int counter = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(textfile));
			try {
				String line;

				while ((line = br.readLine()) != null) {
					try {
						String[] comps = line.split(",");
						map.put(comps[0] + "_" + comps[1], comps[2]);
						counter++;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} finally {
				br.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("read: " + counter);
		return map;
	}

	/**
	 * @param map HashMap uId_fId | rating
	 * @param textfile
	 */
	private static void writeUserpreferencesToFile(HashMap<String, String> map,
			String textfile) {
		int counter = 0;
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(textfile));
			String[] keys = map.keySet().toArray(new String[0]);
			for (String k : keys) {
				try {
					String u = k.substring(0, k.indexOf("_"));
					String f = k.substring(k.indexOf("_") + 1);
					String r = map.get(k);
					if (r != "0"){
						String forFile = u + "," + f + "," + r;
						pw.println(forFile);
						counter++;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			pw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("write: " + counter);
	}

	private String topHeaderNames[];
	private String bottomHeaderNames[];
	private JTable bottomTable;
	private JLabel lRateMin;
	private JPanel bottomPanel;
	
	private void addComponentsToPane(final Container pane) {

		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		JPanel topPanel = new JPanel();
		JPanel midPanel = new JPanel();
		midPanel.setLayout(new BoxLayout(midPanel, BoxLayout.Y_AXIS));
		bottomPanel = new JPanel();
		bottomPanel.setBorder(new EmptyBorder(0,13,0,13));

		// Create top table
		topHeaderNames = new String[] { "id", "Festival", "Location" };
		JTable topTable = new JTable(new TableModel(topTableContent,
				topHeaderNames));
		topTable.setRowSelectionAllowed(false);
		topTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		topTable.getTableHeader().setBackground(Color.cyan);
		topTable.getColumnModel().getColumn(0).setPreferredWidth(30);
		topTable.getColumnModel().getColumn(1).setPreferredWidth(300);
		topTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		topTable.setRowSelectionInterval(0, 0);

		listSelectionModel = topTable.getSelectionModel();
		listSelectionModel
				.addListSelectionListener(new SharedListSelectionHandler());
		topTable.setSelectionModel(listSelectionModel);

		JScrollPane scrollPane = new JScrollPane(topTable);
		scrollPane.setPreferredSize(new Dimension(470, 410));

		// Create midlayer items
		
		lRateFestival.setText("Rate: ");
		final JButton btnMoreInfo = new JButton("More info...");
		JPanel festivalInfoPanel = new JPanel(new FlowLayout());
		festivalInfoPanel.add(lRateFestival);
		festivalInfoPanel.add(btnMoreInfo);
		
		lRateMin = new JLabel();
		lRateMin.setText("Rate more Festivals");

		final ButtonGroup buttonGroup = new ButtonGroup();

		JRadioButton rbOne = new JRadioButton();
		JRadioButton rbTwo = new JRadioButton();
		JRadioButton rbThree = new JRadioButton();
		JRadioButton rbFour = new JRadioButton();
		JRadioButton rbFive = new JRadioButton();
		JRadioButton rbNoRating = new JRadioButton();
		rbNoRating.setSelected(true);

		radioButtons[0] = rbNoRating;
		radioButtons[1] = rbOne;
		radioButtons[2] = rbTwo;
		radioButtons[3] = rbThree;
		radioButtons[4]	= rbFour;
		radioButtons[5] = rbFive;
		radioButtons[0].setActionCommand("0");
		radioButtons[0].setText("No rating");
		for (int i = 1; i < 6; i++) {
			radioButtons[i].setText(i + "");
			radioButtons[i].setActionCommand(i + "");
		}

		rbOne.addActionListener(this);
		rbTwo.addActionListener(this);
		rbThree.addActionListener(this);
		rbFour.addActionListener(this);
		rbFive.addActionListener(this);
		rbNoRating.addActionListener(this);

		JPanel buttonPanel = new JPanel(new FlowLayout());
		for (JRadioButton temp : radioButtons) {
			buttonPanel.add(temp);
		}

		// Create bottom table
		for (int i = 0; i < recs.length; i++) {
			bottomTableContent[i][0] = i + 1 + "";
			bottomTableContent[i][1] = "" + recs[i];
		}

		bottomHeaderNames = new String[] { "Rank", "Festival (id)", "Location" };
		bottomTable = new JTable(new TableModel(bottomTableContent,
				bottomHeaderNames));
		bottomTable.getTableHeader().setBackground(Color.cyan);
		bottomTable.setRowSelectionAllowed(false);


		lRateFestival.setText("Rate: " + topTableContent[0][1]);
		btnMoreInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentURI = "" + topTableContent[currentRow][4];
				try {
					openWebpage(new URI(currentURI));
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

		for (JRadioButton temp : radioButtons) {
			buttonGroup.add(temp);
		}
		
		scrollPane2 = new JScrollPane(bottomTable);
		scrollPane2.setPreferredSize(new Dimension(470, 103));
		
		topPanel.add(scrollPane);
		midPanel.add(festivalInfoPanel);
		midPanel.add(buttonPanel);
		bottomPanel.add(scrollPane2);
		bottomPanel.add(lRateMin);


		pane.add(topPanel);
		pane.add(midPanel);
		pane.add(bottomPanel);

	}

	private String[][] getTableContent(String path) {
		List<String[]> myEntries = new ArrayList<String[]>();
		try {
			CSVReader reader = new CSVReader(new FileReader(path));
			myEntries = reader.readAll();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return myEntries.toArray(new String[0][0]);
	}

	private void updateRecommendedFestivals(){
		userSimilarity(currentUser);
		if (recs.length > 0){
			for (int i = 0; i < recs.length; i++) {
				bottomTableContent[i][0] = i + 1 + "";
				bottomTableContent[i][1] = topTableContent[recs[i]][1]+" (" + recs[i] + ")";
				bottomTableContent[i][2] = topTableContent[recs[i]][2];
			}
			bottomTable.setModel(new TableModel(bottomTableContent,
					bottomHeaderNames));
			bottomTable.getColumnModel().getColumn(0).setPreferredWidth(30);
			bottomTable.getColumnModel().getColumn(1).setPreferredWidth(300);
			bottomTable.getColumnModel().getColumn(2).setPreferredWidth(100);
			bottomTable.revalidate();
			scrollPane2.setVisible(true);
			lRateMin.setVisible(false);
		} else if (recs.length == 0){
			scrollPane2.setVisible(false);
			lRateMin.setVisible(true);
		}
	}
	
	private void userSimilarity(int currentUser) {
		try {

			DataModel dataModel = new FileDataModel(new File(
					USER_PREFERENCES_FILE));

			UserSimilarity userSimilarity = new PearsonCorrelationSimilarity(
					dataModel);

			UserNeighborhood neighborhood = new NearestNUserNeighborhood(3,
					userSimilarity, dataModel);

			recommender = new GenericUserBasedRecommender(dataModel,
					neighborhood, userSimilarity);

			Recommender cachingRecommender = new CachingRecommender(recommender);
			List<RecommendedItem> recommendations = cachingRecommender
					.recommend(currentUser, NUMBER_OF_RECS);
			
			recs = new int[NUMBER_OF_RECS];
			for (int i = 0; i < recs.length; i++) {
				recs[i] = (int) recommendations.get(i).getItemID();
			}
		} catch (IOException e) {
			recs = new int[0];
			e.printStackTrace();
		} catch (TasteException e) {
			recs = new int[0];
			e.printStackTrace();
		} catch (Exception e) {
			recs = new int[0];
			e.printStackTrace();
		}

	}

	public static void openWebpage(URI uri) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop()
				: null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void openWebpage(URL url) {
		try {
			openWebpage(url.toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		currentRating = e.getActionCommand();
		String currentCombination = currentUser + "_" + currentRow;
		if (mapUserPreferences.get(currentCombination) != currentRating) {
			mapUserPreferences.remove(currentCombination);
			mapUserPreferences.put(currentCombination, currentRating);
			writeUserpreferencesToFile(mapUserPreferences,
					USER_PREFERENCES_FILE);
			updateRecommendedFestivals();
		}
	}

	class SharedListSelectionHandler implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			ListSelectionModel lsm = (ListSelectionModel) e.getSource();

			if (lsm.isSelectionEmpty()) {
				System.out.println(" <none>");
			} else {
				// Find out which indexes are selected.
				int minIndex = lsm.getMinSelectionIndex();
				int maxIndex = lsm.getMaxSelectionIndex();
				for (int i = minIndex; i <= maxIndex; i++) {
					if (lsm.isSelectedIndex(i)) {
						currentRow = i;
					}
				}
			}
			
			lRateFestival
					.setText("Rate: " + topTableContent[currentRow][1]);

			if (mapUserPreferences.containsKey(currentUser + "_"
					+ currentRow)) {
				radioButtons[Integer.parseInt(mapUserPreferences
						.get(currentUser + "_" + currentRow))]
						.setSelected(true);
			} else {
				radioButtons[0].setSelected(true);
			}
		}
	}
}
