/**
 * 
 */
package main;

/**
 * @author danilo
 *
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class LoginDialog extends JDialog {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		showDialog(21);
	}
	
	public static void showDialog(int numberOfUsers){
        final JFrame frame = new JFrame("JDialog Demo");
        final JButton btnLogin = new JButton("Click to login");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 100);
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(btnLogin);
        frame.setVisible(true);
        
        LoginDialog loginDlg = new LoginDialog(frame, numberOfUsers);
        loginDlg.setVisible(true);
        if(loginDlg.isCanceled()){
        	//determine program
        }       
        if(loginDlg.isSucceeded()){
        	btnLogin.setText("Hi " + loginDlg.getUsername() + "!");
        }
	}

	private int numberOfUsers;
	private JLabel lbAskUserID;
    private JTextField tfUserID;
    private JLabel lbNewID;
    private JLabel lbYes;
    private JLabel lbNo;
    private JButton btnLogin;
    private JButton btnRegister;
    private JButton btnCancel;
    private boolean succeeded;
    private boolean cancelled;
 
    public LoginDialog(Frame parent, int numberOfUsers) {
        super(parent, "Login", true);
        this.numberOfUsers = numberOfUsers;
        succeeded = false;
        cancelled = false;
        
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        
        cs.fill = GridBagConstraints.HORIZONTAL;
        
        //Question
        int x = 0;
        int width = 0;
        int y = 0;
        lbAskUserID = new JLabel("Do you have a user_ID already?");
        cs.gridx = x;
        cs.gridy = y;
        width = 3;
        x += width;
        cs.gridwidth = width;
        panel.add(lbAskUserID, cs);
        
        //Yes, user has an id
        x = 0;
        y++;
        lbYes = new JLabel("Yes: (1-"+ numberOfUsers + ")");
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(lbYes, cs);
 
        tfUserID = new JTextField(3);
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(tfUserID, cs);
        
        btnLogin = new JButton("Login");
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(btnLogin, cs);
        
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (authenticate(tfUserID.getText())) {
                    JOptionPane.showMessageDialog(LoginDialog.this,
                            "Hi! You are logged in with user_ID:\n" + getUsername() + "",
                            "Login",
                            JOptionPane.INFORMATION_MESSAGE);
                    succeeded = true;
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(LoginDialog.this,
                            "Invalid user_ID",
                            "Login",
                            JOptionPane.ERROR_MESSAGE);
                    // reset username and password
                    tfUserID.setText("");
                    succeeded = false;
                }
            }
        });

        //No, user has no id
        x = 0;
        y++;
        lbNo = new JLabel("No:");
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(lbNo, cs);
 
        lbNewID = new JLabel("" + (numberOfUsers+1));
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(lbNewID, cs);

        btnRegister = new JButton("Register");
        cs.gridx = x;
        cs.gridy = y;
        width = 1;
        x += width;
        cs.gridwidth = width;
        panel.add(btnRegister, cs);
        
        btnRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (registerNewUser()) {
                    JOptionPane.showMessageDialog(LoginDialog.this,
                            "Hi! You are logged in with user_ID:\n" + getUsername() + "\n" +
                            		"Remember your ID!",
                            "Login",
                            JOptionPane.INFORMATION_MESSAGE);
                    succeeded = true;
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(LoginDialog.this,
                            "Invalid user_ID",
                            "Login",
                            JOptionPane.ERROR_MESSAGE);
                    // reset username and password
                    tfUserID.setText("");
                    succeeded = false;
                }
            }
        });

//        panel.setBorder(new LineBorder(Color.GRAY));
        panel.setBorder(new EmptyBorder(20, 20, 20, 20));//Copyright schwonzlutscher plant

        btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	cancelled = true;
                dispose();
            }
        });
        JPanel bp = new JPanel();
        bp.add(btnCancel);
 
        getContentPane().add(panel, BorderLayout.CENTER);
        getContentPane().add(bp, BorderLayout.PAGE_END);
 
        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }
 
    private boolean authenticate(String n){
    	try {
    		int user_id = Integer.parseInt(n);
    		if (user_id > 0 && user_id <= numberOfUsers){
    			return true;	
    		}
    	}catch(Exception e){
    		
    	}
    	return false;
    }
    
    private boolean registerNewUser(){
    	numberOfUsers++;
    	tfUserID.setText("" + numberOfUsers);
    	return true;
    }
    
    public String getUsername() {
        return tfUserID.getText().trim();
    }
 
    public boolean isSucceeded() {
        return succeeded;
    }
    
    public boolean isCanceled() {
    	return cancelled;
    }
}
